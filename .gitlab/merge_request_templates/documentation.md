# Documentation 📚

Thanks for taking a moment to try and improve our documentation! Take a look at our "key concerns" below:

## Key concerns

- ⛑️ Accessibillity issues:
  If you notice anything inaccessible within the documentation please take a moment to open an MR addressing the issue. Let's work together to make the web usable for everyone!
- 🌐 Translations:
  If you or someone you know are able to provide translations for our documentation, please open an MR!

## Nice to haves

- 📱 Responsive design:
  This is kind of a no-brainer in a day where over 50% of web traffic goes through mobile devices. However, being that this is open-source documentation, the user (developer) is most likely to view this on a desktop so it is not a pressing issue.
- 🤦‍♂️ Spelling/Grammar:
  I'm only human. These things happen. Please be sure to point them out in a kind manner!
