# Contributing 🎉

Thanks for wanting to help out, you rock! 😎 Here's how you can get started:

```shell
git clone https://gitlab.com/taylorosbourne/scryfall-graphql.git
cd scryfall-gql

yarn
yarn dev # starts dev server @localhost:5000
```

## Additional ways to contribute:

- 🐛 Report a bug [here](https://gitlab.com/taylorosbourne/scryfall-graphql/-/blob/main/.gitlab/issue_templates/bug.md)
- 📚 Contribute to documentation [here](https://gitlab.com/taylorosbourne/scryfall-graphql/-/blob/main/.gitlab/merge_request_templates/documentation.md)
