<div align="center">
<h1>Scryfall GraphQL</h1>
<p>
  A GraphQL wrapper around the <a href="https://scryfall.com/docs/api">Scryfall API</a>
</p>
</div>

<img src="./assets/banner.png">

### Disclaimer

This project is NOT associated with the folks over at [Scryfall](https://scryfall.com/), just deeply indebted to them 🙌.

## Table of Contents

- [Setting up for local development](#setting-up-for-local-development)
- [Examples](#examples)
- [Terms of service](#terms-of-service)
- [Credits & inspirations](#credits-&-inspirations)
- [Contributors](#contributors)

## Setting up for local development

```shell
git clone https://gitlab.com/taylorosbourne/scryfall-graphql.git
cd scryfall-gql

yarn
yarn dev # starts dev server @localhost:5000
```

Once the dev server is started, you'll be able to view [Graphql Playground](https://github.com/graphql/graphql-playground) at http://localhost:5000.

## Examples

Check out the [examples](https://gitlab.com/taylorosbourne/scryfall-graphql/-/tree/main/examples) directory for examples of the Scryfall GraphQL server in use. Also read [Setting up for local development](#setting-up-for-local-development), to get the dev server started.

## Terms of service

This project just provides a wrapper around the [Scryfall API](https://scryfall.com). The data comes from Scryfall, so their [Terms of Service](https://scryfall.com/docs/terms) should be read before using this, or their, API.

## Credits & inspirations

- [Scryfall](https://scryfall.com) - Stellar search, deckbuilding tools, images, and more for Magic: The Gathering.
- [scryfall-sdk](https://github.com/ChiriVulpes/scryfall-sdk) - A Node.js SDK for https://scryfall.com/docs/api, written in TypeScript.

## Contributors

- Taylor Osbourne, [Gitlab](https://gitlab.com/taylorosbourne), [Website](https://taylorosbourne.com) - creator, maintainer

[Contribute to this project](https://gitlab.com/taylorosbourne/scryfall-graphql/-/blob/main/CONTRIBUTING.md)
