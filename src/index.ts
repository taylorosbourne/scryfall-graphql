import 'dotenv/config';
import { ApolloServer } from 'apollo-server';

import { typeDefs, resolvers } from './graphql';
import { PORT, defaultQuery } from './constants';

const server: ApolloServer = new ApolloServer({
	typeDefs,
	resolvers,
	introspection: true,
	playground: {
		tabs: [
			{
				endpoint: `api.scryfallgql.io`,
				query: defaultQuery,
			},
		],
	},
	context: ({ req, res }) => ({ req, res }),
});

async function main(): Promise<void> {
	try {
		server.listen({ port: PORT }, () =>
			console.log(`🚀 Server running at http://localhost:${PORT} \n`)
		);
	} catch (e) {
		console.error(e);
	}
}

main();
