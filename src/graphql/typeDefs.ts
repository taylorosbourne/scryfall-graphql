import { gql } from 'apollo-server';

const typeDefs = gql`
	enum Unique {
		CARDS
		ART
		PRINTS
	}
	enum Order {
		NAME
		SET
		RELEASED
		RARITY
		COLOR
		USD
		TIX
		EUR
		CMC
		POWER
		TOUGHNESS
		EDH_REC
		ARTIST
	}
	enum Direction {
		AUTO
		ASC
		DESC
	}
	enum SearchFormats {
		JSON
		CSV
	}
	enum NamedFormat {
		JSON
		IMAGE
		TEXT
	}
	enum NamedVersion {
		SMALL
		NORMAL
		LARGE
		PNG
		ART_CROP
		BORDER_CROP
	}
	enum Json {
		JSON
	}
	enum CatalogTypes {
		CARD_NAMES
		ARTIS_NAMES
		WORD_BANK
		CREATURE_TYPES
		PLANESWALKER_TYPES
		LAND_TYPES
		ARTIFACT_TYPES
		ENCHANTMENT_TYPES
		SPELL_TYPES
		POWERS
		TOUGHNESS
		LOYALTIES
		WATERMARKS
		KEYWORD_ABILITIES
		KEYWORD_ACTIONS
		ABILITY_WORDS
	}

	# Field types

	type ImageUris {
		small: String
		normal: String
		large: String
		png: String
		art_crop: String
		border_crop: String
	}
	type CardFaces {
		object: String
		name: String
		mana_cost: String
		type_line: String
		oracle_text: String
		artist: String
		artist_id: String
		illustration_id: String
	}
	type Legalities {
		standard: String
		future: String
		historic: String
		gladiator: String
		pioneer: String
		modern: String
		legacy: String
		pauper: String
		vintage: String
		penny: String
		commander: String
		brawl: String
		duel: String
		oldschool: String
		premodern: String
	}
	type Prices {
		usd: String
		usd_foil: String
		eur: String
		eur_foil: String
		tix: String
	}
	type RelatedUris {
		gatherer: String
		tcgplayer_infinite_articles: String
		tcgplayer_infinite_decks: String
		edhrec: String
		mtgtop8: String
	}

	# Input Options

	input CardByIdOptions {
		format: NamedFormat
		face: String
		version: NamedVersion
		pretty: Boolean
	}

	input SearchOptions {
		unique: Unique
		order: Order
		dir: Direction
		include_extras: Boolean
		include_multilingual: Boolean
		include_variations: Boolean
		page: Int
		format: SearchFormats
		pretty: Boolean
	}
	input NamedSearchOptions {
		exact: String
		fuzzy: String
		set: String
		format: NamedFormat
		face: String
		version: NamedVersion
		pretty: Boolean
	}
	input AutocompleteOptions {
		q: String
		format: String
		pretty: Boolean
		include_extras: Boolean
	}
	input RandomOptions {
		q: String
		format: NamedFormat
		face: String
		version: NamedVersion
		pretty: Boolean
	}
	input SetCodeAndCollectorNumOptions {
		code: String
		number: String
		lang: String
		format: NamedFormat
		version: NamedVersion
		pretty: Boolean
	}
	input UniversalIdOptions {
		id: String
		face: String
		format: NamedFormat
		version: NamedVersion
		pretty: Boolean
	}
	input AllSetsOptions {
		format: String
		pretty: Boolean
	}
	input SetOptions {
		code: String
		format: String
		pretty: Boolean
	}
	input SetIdOptions {
		id: String
		format: String
		pretty: Boolean
	}
	input TcgSetOptions {
		id: String
		format: String
		pretty: Boolean
	}
	input RulingByIdOptions {
		id: String
		format: Json
		pretty: Boolean
	}
	input SetCodeAndCollectorNumRulingsOptions {
		code: String
		number: String
		format: Json
		pretty: Boolean
	}
	input CatalogOptions {
		type: CatalogTypes
		format: Json
		pretty: Boolean
	}

	# Object types

	type Card {
		id: ID!
		object: String
		oracle_id: ID
		multiverse_ids: [Int]
		mtgo_id: Int
		tcgplayer_id: Int
		cardmarket_id: Int
		name: String
		lang: String
		released_at: String
		uri: String
		scryfall_uri: String
		layout: String
		highres_image: Boolean
		image_status: String
		image_uris: ImageUris
		mana_cost: String
		cmc: Float
		type_line: String
		colors: [String]
		color_identity: [String]
		keywords: String
		card_faces: [CardFaces]
		legalities: Legalities
		games: [String]
		reserved: Boolean
		foil: Boolean
		nonfoil: Boolean
		oversized: Boolean
		promo: Boolean
		reprint: Boolean
		variation: Boolean
		set: String
		set_name: String
		set_type: String
		set_uri: String
		set_search_uri: String
		scryfall_set_uri: String
		rulings_uri: String
		prints_search_uri: String
		collector_number: String
		digital: Boolean
		rarity: String
		card_back_id: String
		artist: String
		artist_ids: [String]
		illustration_id: String
		border_color: String
		frame: String
		full_art: Boolean
		textless: Boolean
		booster: Boolean
		story_spotlight: Boolean
		edhrec_rank: Int
		prices: Prices
		related_uris: RelatedUris
	}
	type Set {
		object: String
		id: String
		code: String
		mtgo_code: String
		arena_code: String
		tcgplayer_id: Int
		name: String
		uri: String
		scryfall_uri: String
		search_uri: String
		released_at: String
		set_type: String
		card_count: Int
		printed_size: Int
		digital: Boolean
		nonfoil_only: Boolean
		foil_only: Boolean
		block_code: String
		block: String
		icon_svg_uri: String
	}
	type Ruling {
		object: String
		oracle_id: String
		source: String
		published_at: String
		comment: String
	}
	type Symbol {
		object: String
		symbol: String
		svg_uri: String
		loose_variant: String
		english: String
		transposable: Boolean
		represents_mana: Boolean
		appears_in_mana_costs: Boolean
		cmc: String
		funny: Boolean
		colors: [String]
		gatherer_alternates: [String]
	}
	type Catalog {
		object: String
		uri: String
		total_values: String
		data: [String]
	}

	# Result Types

	type AllSetResults {
		object: String
		has_more: Boolean
		data: [Set]
	}
	type AllSymbolsResults {
		object: String
		has_more: Boolean
		data: [Symbol]
	}
	type SearchResults {
		object: String
		total_cards: Int
		has_more: Boolean
		data: [Card]
	}
	type AutocompleteResults {
		object: String
		total_values: Int
		data: [String]
	}
	type CardRulingResults {
		object: String
		has_more: Boolean
		data: [Ruling]
	}

	type Query {
		searchCards(q: String!, options: SearchOptions): SearchResults
		getCardById(id: String!, options: CardByIdOptions): Card
		getNamedCard(options: NamedSearchOptions): Card
		getAutocompleteCard(options: AutocompleteOptions): AutocompleteResults
		getRandomCard(options: RandomOptions): Card
		getCardBySetCodeAndCollectorNum(
			options: SetCodeAndCollectorNumOptions
		): Card
		getCardByMultiverseId(options: UniversalIdOptions): Card
		getCardByMtgoId(options: UniversalIdOptions): Card
		getCardByArenaId(options: UniversalIdOptions): Card
		getCardByTcgPlayerId(options: UniversalIdOptions): Card
		getCardByCardmarketId(options: UniversalIdOptions): Card

		getAllSets(options: AllSetsOptions): AllSetResults
		getSetById(options: SetIdOptions): Set
		getSetByCode(options: SetOptions): Set
		getSetByTcgPlayerId(options: TcgSetOptions): Set

		getRulingsByCardId(options: RulingByIdOptions): CardRulingResults
		getRulingsByMultiverseId(options: RulingByIdOptions): CardRulingResults
		getRulingsByMtgoId(options: RulingByIdOptions): CardRulingResults
		getRulingsByArenaId(options: RulingByIdOptions): CardRulingResults
		getRulingsBySetCodeAndCollectorNum(
			options: SetCodeAndCollectorNumRulingsOptions
		): CardRulingResults

		getAllSymbols: AllSymbolsResults

		getCatalog(options: CatalogOptions): Catalog
	}
`;

export default typeDefs;
