export type Symbol = {
	object: string;
	symbol: string;
	svg_uri: string;
	loose_variant: string;
	english: string;
	transposable: boolean;
	represents_mana: boolean;
	appears_in_mana_costs: boolean;
	cmc: string;
	funny: boolean;
	colors: string[];
	gatherer_alternates: string[];
};

export interface SymbolResponse {
	object: string;
	has_more: boolean;
	data: Symbol[];
}
