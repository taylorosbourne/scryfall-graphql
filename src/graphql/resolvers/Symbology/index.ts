import fetch from 'node-fetch';

import { SymbolResponse } from './types';
import { baseUrl } from '../../../constants';

export default {
	Query: {
		getAllSymbols: async (): Promise<SymbolResponse> => {
			const res = await fetch(`${baseUrl}/symbology`);
			return res.json();
		},
	},
};
