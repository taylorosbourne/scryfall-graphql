import fetch from 'node-fetch';

import {
	Card,
	NamedOptions,
	SearchOptions,
	SearchResults,
	SearchCsvResults,
	AutocompleteOptions,
	AutocompleteResults,
	RandomOptions,
	SetCodeAndCollectorNumOptions,
	UniversalIdOptions,
	CardByIdOptions,
} from './types';
import { buildQueryString } from '../../../utils';
import { baseUrl } from '../../../constants';
import { Format } from '../../../types';

export default {
	Query: {
		getCardById: async (
			_: any,
			{ id, options }: CardByIdOptions
		): Promise<Card> => {
			const url = buildQueryString(
				`${baseUrl}/cards/${id}?`,
				options ? options : { format: Format.JSON }
			);
			const res = await fetch(url);
			return res.json();
		},
		searchCards: async (
			_: any,
			{ q, options }: SearchOptions
		): Promise<SearchResults | SearchCsvResults> => {
			const url = buildQueryString(
				`${baseUrl}/cards/search?q=${q}&`,
				options ? options : { format: Format.JSON }
			);
			const res = await fetch(url);
			if (options?.format) {
				if (String(options?.format).toLowerCase() !== String(Format.JSON)) {
					return res;
				}
			}
			return res.json();
		},
		getNamedCard: async (
			_: any,
			{ options }: NamedOptions
		): Promise<Card | SearchCsvResults> => {
			const url = buildQueryString(
				`${baseUrl}/cards/named?`,
				options ? options : { format: Format.JSON }
			);
			const res = await fetch(url);
			if (options?.format) {
				if (String(options.format).toLowerCase() !== String(Format.JSON)) {
					return res;
				}
			}
			return res.json();
		},
		getAutocompleteCard: async (
			_: any,
			{ options }: AutocompleteOptions
		): Promise<AutocompleteResults> => {
			const url = buildQueryString(`${baseUrl}/cards/autocomplete?`, options);
			const res = await fetch(url);
			return res.json();
		},
		getRandomCard: async (
			_: any,
			{ options }: RandomOptions
		): Promise<Card> => {
			const url = buildQueryString(`${baseUrl}/cards/random?`, options);
			const res = await fetch(url);
			return res.json();
		},
		getCardBySetCodeAndCollectorNum: async (
			_: any,
			{ options }: SetCodeAndCollectorNumOptions
		): Promise<Card> => {
			const { code, number, lang, format, version, pretty } = options;
			const remainingOpts = {
				format,
				version,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/${code}/${number}${lang ? `/lang?` : `?`}`,
				remainingOpts
			);
			const res = await fetch(url);
			return res.json();
		},
		getCardByMultiverseId: async (
			_: any,
			{ options }: UniversalIdOptions
		): Promise<Card> => {
			const { id, format, face, version, pretty } = options;
			const remainingOpts = {
				format,
				face,
				version,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/multiverse/${id}`,
				remainingOpts
			);
			const res = await fetch(url);
			return res.json();
		},
		getCardByMtgoId: async (
			_: any,
			{ options }: UniversalIdOptions
		): Promise<Card> => {
			const { id, format, face, version, pretty } = options;
			const remainingOpts = {
				format,
				face,
				version,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/mtgo/${id}`,
				remainingOpts
			);
			const res = await fetch(url);
			return res.json();
		},
		getCardByArenaId: async (
			_: any,
			{ options }: UniversalIdOptions
		): Promise<Card> => {
			const { id, format, face, version, pretty } = options;
			const remainingOpts = {
				format,
				face,
				version,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/arena/${id}`,
				remainingOpts
			);
			const res = await fetch(url);
			return res.json();
		},
		getCardByTcgPlayerId: async (
			_: any,
			{ options }: UniversalIdOptions
		): Promise<Card> => {
			const { id, format, face, version, pretty } = options;
			const remainingOpts = {
				format,
				face,
				version,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/tcgplayer/${id}`,
				remainingOpts
			);
			const res = await fetch(url);
			return res.json();
		},
		getCardByCardmarketId: async (
			_: any,
			{ options }: UniversalIdOptions
		): Promise<Card> => {
			const { id, format, face, version, pretty } = options;
			const remainingOpts = {
				format,
				face,
				version,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/cardmarket/${id}`,
				remainingOpts
			);
			const res = await fetch(url);
			return res.json();
		},
	},
};
