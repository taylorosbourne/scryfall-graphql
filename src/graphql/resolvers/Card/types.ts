import { Format } from '../../../types';

export type Card = {
	id: string;
	object: string;
	oracle_id: string;
	multiverse_ids: number[];
	mtgo_id: number;
	tcgplayer_id: number;
	cardmarket_id: number;
	name: string;
	lang: string;
	released_at: string;
	uri: string;
	scryfall_uri: string;
	layout: string;
	highres_image: boolean;
	image_status: string;
	image_uris: ImageUris;
	mana_cost: string;
	cmc: number;
	type_line: string;
	colors: string[];
	color_identity: string[];
	keywords: string;
	card_faces: CardFaces[];
	legalities: Legalities;
	games: string[];
	reserved: boolean;
	foil: boolean;
	nonfoil: boolean;
	oversized: boolean;
	promo: boolean;
	reprint: boolean;
	variation: boolean;
	set: string;
	set_name: string;
	set_type: string;
	set_uri: string;
	set_search_uri: string;
	scryfall_set_uri: string;
	rulings_uri: string;
	prints_search_uri: string;
	collector_number: string;
	digital: boolean;
	rarity: string;
	card_back_id: string;
	artist: string;
	artist_ids: string[];
	illustration_id: string;
	border_color: string;
	frame: string;
	full_art: boolean;
	textless: boolean;
	booster: boolean;
	story_spotlight: boolean;
	edhrec_rank: number;
	prices: Prices;
	related_uris: RelatedUris;
};

export enum Order {
	NAME = 'name',
	SET = 'set',
	RELEASED = 'released',
	RARITY = 'rarity',
	COLOR = 'color',
	USD = 'usd',
	TIX = 'tix',
	EUR = 'eur',
	CMC = 'cmc',
	POWER = 'power',
	TOUGHNESS = 'toughness',
	EDH_REC = 'edhrec',
	ARTIST = 'artist',
}

export enum SearchFormat {
	JSON = Format.JSON,
	CSV = Format.CSV,
}

export enum NamedFormat {
	JSON = Format.JSON,
	TEXT = Format.TEXT,
	IMAGE = Format.IMAGE,
}

export enum Unique {
	CARDS = 'cards',
	ART = 'art',
	PRINTS = 'prints',
}

export enum Direction {
	AUTO = 'auto',
	ASC = 'asc',
	DESC = 'desc',
}

export enum Version {
	SMALL = 'small',
	NORMAL = 'normal',
	LARGE = 'large',
	PNG = 'png',
	ART_CROP = 'art_crop',
	BORDER_CROP = 'border_crop',
}

interface ImageUris {
	small: string;
	normal: string;
	large: string;
	png: string;
	art_crop: string;
	border_crop: string;
}

export interface CardFaces {
	object: string;
	name: string;
	mana_cost: string;
	type_line: string;
	oracle_text: string;
	artist: string;
	artist_id: string;
	illustration_id: string;
}

export interface Legalities {
	standard: string;
	future: string;
	historic: string;
	gladiator: string;
	pioneer: string;
	modern: string;
	legacy: string;
	pauper: string;
	vintage: string;
	penny: string;
	commander: string;
	brawl: string;
	duel: string;
	oldschool: string;
	premodern: string;
}

export interface Prices {
	usd: string;
	usd_foil: string;
	eur: string;
	eur_foil: string;
	tix: string;
}

export interface RelatedUris {
	gatherer: string;
	tcgplayer_infinite_articles: string;
	tcgplayer_infinite_decks: string;
	edhrec: string;
	mtgtop8: string;
}

export interface SearchOptions {
	q: string;
	options?: {
		unique: Unique;
		order: Order;
		dir: Direction;
		include_extras: boolean;
		include_multilingual: boolean;
		include_variations: boolean;
		page: number;
		format: SearchFormat;
		pretty: boolean;
	};
}

export interface NamedOptions {
	options: {
		exact: string;
		fuzzy: string;
		set: string;
		format: NamedFormat;
		face: string;
		version: Version;
		pretty: boolean;
	};
}

export interface AutocompleteOptions {
	options: {
		q: string;
		format: Format.JSON;
		pretty: boolean;
		include_extras: boolean;
	};
}

export interface RandomOptions {
	options: {
		q: string;
		format: Format;
		face: string;
		version: Version;
		pretty: boolean;
	};
}

export interface SetCodeAndCollectorNumOptions {
	options: {
		code: string;
		number: string;
		lang: string;
		format: Format;
		version: Version;
		pretty: boolean;
	};
}

export interface UniversalIdOptions {
	options: {
		id: string;
		format: Format;
		face: string;
		version: Version;
		pretty: boolean;
	};
}

export interface CardByIdOptions {
	id: string;
	options?: {
		format: Format;
		face: string;
		version: Version;
		pretty: boolean;
	};
}

export interface SearchResults {
	object: string;
	total_cards: number;
	has_more: boolean;
	data: Card[];
}

export type SearchCsvResults = any;

export interface AutocompleteResults {
	object: string;
	total_values: number;
	data: string[];
}
