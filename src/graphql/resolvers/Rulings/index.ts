import fetch from 'node-fetch';

import {
	Ruling,
	RulingsByIdOptions,
	RulingsBySetCodeAndCollectorNumOptions,
} from './types';
import { buildQueryString } from '../../../utils';
import { baseUrl } from '../../../constants';

export default {
	Query: {
		getRulingsByCardId: async (
			_: any,
			{ options }: RulingsByIdOptions
		): Promise<Ruling> => {
			const { id, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/${id}/rulings?`,
				remainingOptions
			);
			const res = await fetch(url);
			return res.json();
		},
		getRulingsByMultiverseId: async (
			_: any,
			{ options }: RulingsByIdOptions
		): Promise<Ruling> => {
			const { id, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/multiverse/${id}/rulings?`,
				remainingOptions
			);
			const res = await fetch(url);
			return res.json();
		},
		getRulingsByMtgoId: async (
			_: any,
			{ options }: RulingsByIdOptions
		): Promise<Ruling> => {
			const { id, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/mtgo/${id}/rulings?`,
				remainingOptions
			);
			const res = await fetch(url);
			return res.json();
		},
		getRulingsByArenaId: async (
			_: any,
			{ options }: RulingsByIdOptions
		): Promise<Ruling> => {
			const { id, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/arena/${id}/rulings?`,
				remainingOptions
			);
			const res = await fetch(url);
			return res.json();
		},
		getRulingsBySetCodeAndCollectorNum: async (
			_: any,
			{ options }: RulingsBySetCodeAndCollectorNumOptions
		): Promise<Ruling> => {
			const { code, number, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/cards/${code}/${number}/rulings?`,
				remainingOptions
			);
			const res = await fetch(url);
			return res.json();
		},
	},
};
