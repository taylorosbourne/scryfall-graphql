import { Format } from '../../../types';

export type Ruling = {
	object: string;
	oracle_id: string;
	source: string;
	published_at: string;
	comment: string;
};

export interface RulingsByIdOptions {
	options: {
		id: string;
		format: Format.JSON;
		pretty: boolean;
	};
}

export interface RulingsBySetCodeAndCollectorNumOptions {
	options: {
		code: string;
		number: string;
		format: Format.JSON;
		pretty: boolean;
	};
}
