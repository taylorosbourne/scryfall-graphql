import cardResolvers from './Card';
import setResolvers from './Set';
import rulingResolvers from './Rulings';
import symbologyResolvers from './Symbology';
import catalogResolvers from './Catalog';
import additionalResolvers from './additionalResolvers';

export default {
	...additionalResolvers,
	Query: {
		...cardResolvers.Query,
		...setResolvers.Query,
		...rulingResolvers.Query,
		...symbologyResolvers.Query,
		...catalogResolvers.Query,
	},
};
