import { Format } from '../../../types';

export enum CatalogTypes {
	CARD_NAMES = 'card-names',
	ARTIS_NAMES = 'artist-names',
	WORD_BANK = 'word-bank',
	CREATURE_TYPES = 'creature-types',
	PLANESWALKER_TYPES = 'planeswalker-types',
	LAND_TYPES = 'land-types',
	ARTIFACT_TYPES = 'artifact-types',
	ENCHANTMENT_TYPES = 'enchantment-types',
	SPELL_TYPES = 'spell-types',
	POWERS = 'powers',
	TOUGHNESS = 'toughnesses',
	LOYALTIES = 'loyalties',
	WATERMARKS = 'watermarks',
	KEYWORD_ABILITIES = 'keyword-abilities',
	KEYWORD_ACTIONS = 'keyword-actions',
	ABILITY_WORDS = 'ability-words',
}

export type Catalog = {
	object: string;
	uri: string;
	total_values: string;
	data: string[];
};

export interface CatalogOptions {
	options: {
		type: CatalogTypes;
		format: Format.JSON;
		pretty: boolean;
	};
}
