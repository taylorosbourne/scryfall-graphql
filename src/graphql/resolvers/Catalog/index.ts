import fetch from 'node-fetch';

import { Catalog, CatalogOptions } from './types';
import { baseUrl } from '../../../constants';
import { buildQueryString } from '../../../utils';

export default {
	Query: {
		getCatalog: async (
			_: any,
			{ options }: CatalogOptions
		): Promise<Catalog> => {
			const { type, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/catalog/${type}?`,
				remainingOptions
			);
			const res = await fetch(url);
			return res.json();
		},
	},
};
