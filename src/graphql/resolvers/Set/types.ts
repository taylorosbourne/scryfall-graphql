import { Format } from '../../../types';

export type Set = {
	object: string;
	id: string;
	code: string;
	mtgo_code: string;
	arena_code: string;
	tcgplayer_id: number;
	name: string;
	uri: string;
	scryfall_uri: string;
	search_uri: string;
	released_at: string;
	set_type: string;
	card_count: number;
	printed_size: number;
	digital: boolean;
	nonfoil_only: boolean;
	foil_only: boolean;
	block_code: string;
	block: string;
	icon_svg_uri: string;
};

export interface SetsOptions {
	options: {
		format: Format.JSON;
		pretty: boolean;
	};
}

export interface SetOptions {
	options: {
		code: string;
		format: Format.JSON;
		pretty: boolean;
	};
}

export interface TcgSetOptions {
	options: {
		id: string;
		format: Format.JSON;
		pretty: boolean;
	};
}

export interface SetIdOptions {
	options: {
		id: string;
		format: Format.JSON;
		pretty: boolean;
	};
}
