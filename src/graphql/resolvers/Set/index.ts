import fetch from 'node-fetch';

import {
	Set,
	SetsOptions,
	SetOptions,
	TcgSetOptions,
	SetIdOptions,
} from './types';
import { buildQueryString } from '../../../utils';
import { baseUrl } from '../../../constants';

export default {
	Query: {
		getAllSets: async (_: any, { options }: SetsOptions): Promise<Set[]> => {
			const url = buildQueryString(`${baseUrl}/sets`, options);
			const res = await fetch(url);
			return res.json();
		},
		getSetByCode: async (_: any, { options }: SetOptions): Promise<Set[]> => {
			const { code, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(`${baseUrl}/sets/${code}`, remainingOptions);
			const res = await fetch(url);
			return res.json();
		},
		getSetById: async (_: any, { options }: SetIdOptions): Promise<Set[]> => {
			const { id, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(`${baseUrl}/sets/${id}`, remainingOptions);
			const res = await fetch(url);
			return res.json();
		},
		getSetByTcgPlayerId: async (
			_: any,
			{ options }: TcgSetOptions
		): Promise<Set[]> => {
			const { id, format, pretty } = options;
			const remainingOptions = {
				format,
				pretty,
			};
			const url = buildQueryString(
				`${baseUrl}/sets/tcgplayer/${id}`,
				remainingOptions
			);
			const res = await fetch(url);
			return res.json();
		},
	},
};
