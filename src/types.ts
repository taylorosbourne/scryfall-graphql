export enum Format {
	JSON = 'json',
	IMAGE = 'image',
	TEXT = 'text',
	CSV = 'csv',
}
