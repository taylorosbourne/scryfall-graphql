export const PORT: string | number = process.env.PORT || 5000;
export const baseUrl = 'https://api.scryfall.com';
export const defaultQuery = `query SearchCards {
  searchCards(q: "color>=uw -c:green") {
    total_cards
    data {
      id
      name
      cmc
      colors
    }
  }
}
`;
