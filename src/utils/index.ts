export const buildQueryString = (url: string, options: any) => {
	let newUrl: string = url;
	const remainingOptions: string[] = Object.keys(options);
	remainingOptions.forEach((opt: string, i: number): void => {
		// @ts-ignore
		if (options[`${opt}`]) {
			newUrl +=
				newUrl === url
					? // @ts-ignore
					  `${opt}=${options[`${opt}`]}`
					: // @ts-ignore
					  `&${opt}=${options[`${opt}`]}`;
		}
	});
	return newUrl;
};
