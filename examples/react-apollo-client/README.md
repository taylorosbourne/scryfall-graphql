# React Apollo Client Example

An example project showing the use of the Scryfall GraphQL server in a React app with [Apollo Client](https://www.apollographql.com/docs/react/)

<img src="./src/assets/screenshot.png" />

## Don't forget

Run the Scryfall GraphQL dev server in another terminal window. This example is expecting to find the server running at http://localhost:5000

## Available Scripts

In the project directory, you can run:

- `yarn start` - runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
- `yarn test` - runs available test suite.
- `yarn build` - builds the app for production to the `build` folder.
- `yarn eject` - exposes content of `react-script` package
- `yarn lint` - lints project files.
