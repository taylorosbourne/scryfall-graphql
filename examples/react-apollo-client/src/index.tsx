import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { useLazyQuery } from '@apollo/client';
import { ApolloProvider } from '@apollo/client/react';

import { client } from './client';
import { SEARCH_CARDS } from './queries';
import Layout from './layout';

function App() {
	const [searchVal, setSearchVal] = React.useState(`color>=uw -c:red`);
	const [searchCards, { loading, error, data }] = useLazyQuery(SEARCH_CARDS);

	if (loading)
		return (
			<Layout>
				<div className="lds-ripple">
					<div />
					<div />
				</div>
			</Layout>
		);
	if (error) return <Layout>Error...{error.message}</Layout>;

	return (
		<Layout
			searchVal={searchVal}
			setSearchVal={setSearchVal}
			searchCards={searchCards}
		>
			{data && (
				<pre>
					<code lang={`json`}>{JSON.stringify(data, null, 2)}</code>
				</pre>
			)}
		</Layout>
	);
}

ReactDOM.render(
	<ApolloProvider client={client}>
		<App />
	</ApolloProvider>,
	document.getElementById('root')
);

serviceWorker.unregister();
