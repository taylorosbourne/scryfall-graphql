import React from 'react';

import { InputContainer, StyledInput } from './styles';

export interface InputProps {
	value: string;
	type?: string;
	placeholder?: string;
	style?: {};
	onChange?: (e?: any) => void;
}

export default function Input({
	value,
	type = 'text',
	placeholder = 'Type...',
	style,
	onChange,
}: InputProps) {
	return (
		<InputContainer>
			<StyledInput
				value={value}
				placeholder={placeholder}
				type={type}
				style={style}
				onChange={onChange}
			/>
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="24"
				height="24"
				viewBox="0 0 24 24"
				fill="none"
				stroke="currentColor"
				stroke-width="2"
				stroke-linecap="round"
				stroke-linejoin="round"
				className="feather feather-search"
			>
				<circle cx="11" cy="11" r="8" />
				<line x1="21" y1="21" x2="16.65" y2="16.65" />
			</svg>
		</InputContainer>
	);
}
