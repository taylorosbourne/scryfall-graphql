import React, { FormEvent } from 'react';

import { Input } from './components';
import { Planet } from './assets';
import { GlobalStyle } from './styles';

interface LayoutProps {
	searchVal?: string;
	setSearchVal?: (str: string) => void;
	searchCards?: any;
	children?: any;
}

export default function Layout({
	searchVal = ``,
	setSearchVal,
	searchCards,
	children,
}: LayoutProps) {
	function handleSubmit(e: FormEvent) {
		e.preventDefault();
		searchCards({ variables: { q: searchVal } });
	}
	return (
		<>
			<GlobalStyle />
			<img src={Planet} alt={`logo`} />
			<form onSubmit={(e) => handleSubmit(e)}>
				<Input
					value={searchVal}
					style={{ paddingLeft: `35px` }}
					onChange={
						setSearchVal ? (e) => setSearchVal(e.target.value) : undefined
					}
				/>
			</form>
			<p>
				You can use{' '}
				<a href={`https://scryfall.com/docs/syntax`}>Scryfall syntax</a> to
				search in the input above
			</p>
			<p id={`fine-print`}>
				Edit src/queries.ts to change the data returned from this query
			</p>
			{children}
		</>
	);
}
