import React, { FormEvent } from 'react';

import { Input } from './components';
import { Planet } from './assets';
import { GlobalStyle } from './styles';

interface LayoutProps {
	searchVal: string;
	setSearchVal: (str: string) => void;
	setSearched: (bool: boolean) => void;
	children: any;
}

export default function Layout({
	searchVal = ``,
	setSearchVal,
	setSearched,
	children,
}: LayoutProps) {
	function handleSubmit(e: FormEvent) {
		e.preventDefault();
		setSearched(true);

		if (document?.activeElement) {
			// @ts-ignore
			document.activeElement.blur();
		}
	}
	return (
		<>
			<GlobalStyle />
			<img src={Planet} alt={`logo`} />
			<form onSubmit={(e) => handleSubmit(e)}>
				<Input
					onFocus={() => setSearched(false)}
					value={searchVal}
					style={{ paddingLeft: `35px` }}
					onChange={(e) => setSearchVal(e.target.value)}
				/>
			</form>
			<p>
				You can use{' '}
				<a href={`https://scryfall.com/docs/syntax`}>Scryfall syntax</a> to
				search in the input above
			</p>
			<p id={`fine-print`}>
				Edit src/queries.ts to change the data returned from this query
			</p>
			{children}
		</>
	);
}
