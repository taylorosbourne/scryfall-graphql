import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
	html, body {
    overflow: hidden;
    background-attachment: fixed;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
		background-image: linear-gradient(
			to top,
		#051937,
		#2d265c,
		#642c78,
		#a22485,
		#e00082
		);
    color: #ffffff;
    font-family: Arial, Helvetica, sans-serif;
	}
  div#root {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100vh;
    width: 100vw;
  }
  pre {
    max-height: 60vh;
    overflow-y: scroll;
    background: #081229;
    padding: 22px;
    border-radius: 6px;
    color: #ffffff;
    font-size: 1.25rem;
    animation: 0.3s ease-in-out slideIn;
    code {
      font-family: monospace;
    }
  }
  img {
    width: 300px;
  }
  p {
    color: #ffffff;
    font-family: 'Arial';
    line-height: 1.5;
    font-size: 1.5rem;
    a {
      color: #ffffff;
      text-decoration: none;
      font-weight: bold;
      border-bottom: 3px rgb(116, 97, 117) dashed;
    }
  }
  p#fine-print {
    font-size: 1rem;
		font-weight: 100;
		font-style: italic;
		margin: 0 auto;
  }
  .lds-ripple {
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-ripple div {
    position: absolute;
    border: 4px solid #fff;
    opacity: 1;
    border-radius: 50%;
    animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
  }
  .lds-ripple div:nth-child(2) {
    animation-delay: -0.5s;
  }
  @keyframes lds-ripple {
    0% {
      top: 36px;
      left: 36px;
      width: 0;
      height: 0;
      opacity: 1;
    }
    100% {
      top: 0px;
      left: 0px;
      width: 72px;
      height: 72px;
      opacity: 0;
    }
  }
  @keyframes slideIn {
    0% {
      transform: translateY(200px);
    }
    100% {
      transform: translateY(0);
    }
  }
`;
