import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'urql';

import { Data } from './components';
import { client } from './client';
import Layout from './layout';

function App() {
	const [searched, setSearched] = useState(false);
	const [searchVal, setSearchVal] = useState(`color>=uw -c:red`);

	return (
		<Layout
			searchVal={searchVal}
			setSearchVal={setSearchVal}
			setSearched={setSearched}
		>
			{searched && <Data searchVal={searchVal} />}
		</Layout>
	);
}

ReactDOM.render(
	<Provider value={client}>
		<App />
	</Provider>,
	document.getElementById('root')
);

serviceWorker.unregister();
