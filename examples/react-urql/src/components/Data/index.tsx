import * as React from 'react';
import { useQuery } from 'urql';

import { SEARCH_CARDS } from '../../queries';

interface DataProps {
	searchVal: string;
}

export default function Data({ searchVal }: DataProps) {
	const [result] = useQuery({
		query: SEARCH_CARDS,
		variables: { q: searchVal },
	});

	const { data, fetching, error } = result;

	if (fetching)
		return (
			<>
				<div className="lds-ripple">
					<div />
					<div />
				</div>
			</>
		);
	if (error) return <p>Error...{error.message}</p>;
	return (
		<>
			{data && (
				<pre>
					<code lang={`json`}>{JSON.stringify(data, null, 2)}</code>
				</pre>
			)}
		</>
	);
}
