import styled from 'styled-components';

export const InputContainer = styled.div`
	position: relative;
	svg {
		position: absolute;
		top: 34%;
		left: 5px;
		color: rgb(142, 142, 142);
	}
`;

export const StyledInput = styled.input`
	background: rgb(222, 222, 222);
	color: rgb(102, 102, 102);
	font-size: 1.25rem;
	line-height: 1.5;
	padding: 4px 6px;
	border-radius: 4px;
	border: 1px solid rgb(142, 142, 142);
	min-width: 400px;
	margin: 15px auto;
`;
