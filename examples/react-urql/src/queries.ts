export const SEARCH_CARDS = `
	query SearchCards($q: String!) {
		searchCards(options: { q: $q, order: RARITY }) {
			# Here you can determine what is returned from the server
			total_cards
			data {
				id
				name
				cmc
			}
		}
	}
`;
